import {
  SET_ALL_GLOBAL_SETTINGS,
  SET_GLOBAL_THEME,
  SET_GLOBAL_MAX_DISTANCE_FROM_DESTINATION,
  SET_GLOBAL_MAX_NUMBER_OF_PARKING_LOTS,
} from '../actions/types';

import themes from '../themes';

const INITIAL_STATE = {
  theme: themes.light,
  maxDistanceFromDestination: 2,
  maxNumberOfParkingLots: 5,
  isReady: false,
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_ALL_GLOBAL_SETTINGS:
      return {
        ...state,
        theme: themes[action.payload.theme],
        maxDistanceFromDestination: action.payload.maxDistanceFromDestination,
        maxNumberOfParkingLots: action.payload.maxNumberOfParkingLots,
        isReady: true,
      };
    case SET_GLOBAL_THEME:
      return { ...state, theme: themes[action.payload] };
    case SET_GLOBAL_MAX_DISTANCE_FROM_DESTINATION:
      return { ...state, maxDistanceFromDestination: action.payload };
    case SET_GLOBAL_MAX_NUMBER_OF_PARKING_LOTS:
      return { ...state, maxNumberOfParkingLots: action.payload };
    default:
      return state;
  }
}
