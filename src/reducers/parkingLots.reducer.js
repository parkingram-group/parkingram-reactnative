import {
  SET_PARKING_LOT,
  SET_USER_ALERTED,
  SET_ARRIVED_TO_PARKING_LOT,
  UPDATED_PARKING_LOT_CROWDEDNESS,
  SEND_DESTINATION_TO_SERVER_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  parkingLotResults: [],
  parkingLot: null,
  didArrive: false,
  alerted: false,
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SEND_DESTINATION_TO_SERVER_SUCCESS:
      return { ...state, parkingLotResults: action.payload, didArrive: false, alerted: false };
    case SET_PARKING_LOT:
      return {
        parkingLotResults: action.payload ? [] : state.parkingLotResults,
        parkingLot: action.payload,
        didArrive: false,
        alerted: false,
      };
    case SET_ARRIVED_TO_PARKING_LOT:
      return { ...state, didArrive: action.payload, alerted: false };
    case SET_USER_ALERTED:
      return { ...state, alerted: action.payload };
    case UPDATED_PARKING_LOT_CROWDEDNESS:
      return {
        ...state,
        parkingLot: null,
        didArrive: false,
        alerted: false,
      };
    default:
      return state;
  }
}
