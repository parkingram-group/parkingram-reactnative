import { combineReducers } from 'redux';
import location from './location.reducer';
import settings from './settings.reducer';
import parkingLots from './parkingLots.reducer';

const rootReducer = combineReducers({
  location,
  settings,
  parkingLots,
});

export default rootReducer;
