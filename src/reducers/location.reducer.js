import {
  GET_CURRENT_LOCATION_REQUEST,
  GET_CURRENT_LOCATION_ERROR,
  GET_CURRENT_LOCATION_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  isLoading: false,
  error: null,
  region: null,
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_CURRENT_LOCATION_REQUEST:
      return { ...state, isLoading: true };
    case GET_CURRENT_LOCATION_ERROR:
      return { ...state, isLoading: false, error: action.payload };
    case GET_CURRENT_LOCATION_SUCCESS:
      return { ...state, isLoading: false, region: action.payload };
    default:
      return state;
  }
}
