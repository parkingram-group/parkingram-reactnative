import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Location } from 'expo';
import { Container, Content, Spinner } from 'native-base';
import GooglePlacesInput from '../containers/GooglePlacesInput';
import MapContainer from '../containers/MapContainer';
import ParkingLotList from '../containers/ParkingLotList';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    if (this.props.isLoading) {
      return (
        <Container style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Content>
            <Spinner />
          </Content>
        </Container>
      );
    }
    if (this.props.error) {
      Location.requestPermissionsAsync();
      return (
        <Container style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Content>
            <Text style={styles.error}>{this.props.error}</Text>
          </Content>
        </Container>
      );
    }
    return (
      <Container>
        <MapContainer />
        <GooglePlacesInput />
        <ParkingLotList />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  error: {
    backgroundColor: 'rgba(200, 20, 0, 0.5)',
    borderRadius: 20,
    paddingHorizontal: 18,
    paddingVertical: 12,
  },
});
