import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Font, SplashScreen } from 'expo';

import { initAllGlobalSettings, getCurrentLocation } from '../actions';

class FakeSplashScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    SplashScreen.preventAutoHide();
    this.state = {
      fontsLoaded: false,
    };
  }

  componentDidMount() {
    this.props.initAllGlobalSettings();
    this.props.getCurrentLocation();
    Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    }).then(() => {
      this.setState({ fontsLoaded: true });
    });
  }

  componentDidUpdate() {
    if (this.props.isSettingsReady) {
      if (
        this.props.region &&
        this.props.maxDistanceFromDestination &&
        this.props.maxNumberOfParkingLots &&
        this.state.fontsLoaded
      ) {
        SplashScreen.hide();
        this.props.navigation.navigate('App');
      }
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    isSettingsReady: state.settings.isReady,
    maxDistanceFromDestination: state.settings.maxDistanceFromDestination,
    maxNumberOfParkingLots: state.settings.maxNumberOfParkingLots,
    region: state.location.region,
    error: state.location.error,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      initAllGlobalSettings,
      getCurrentLocation,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FakeSplashScreen);
