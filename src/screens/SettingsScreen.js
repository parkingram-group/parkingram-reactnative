import React from 'react';
import { Platform } from 'react-native';
import {
  Container,
  Content,
  ListItem,
  Text,
  Body,
  Left,
  Right,
  Switch,
  Icon,
  Item,
} from 'native-base';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NumberInput from '../components/NumberInput';

import {
  setGlobalTheme,
  setGlobalMaxDistanceFromDestination,
  setGlobalMaxNumberOfParkingLots,
} from '../actions';

class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Settings',
  };

  changeTheme = () => {
    const newTheme = this.props.theme === 'light' ? 'dark' : 'light';
    this.props.setGlobalTheme(newTheme);
  };

  changeMaxDistanceFromDestination = value => {
    if (value) this.props.setGlobalMaxDistanceFromDestination(value);
  };

  changeMaxNumberOfParkingLots = value => {
    if (value) this.props.setGlobalMaxNumberOfParkingLots(value);
  };

  render() {
    return (
      <Container>
        <Content>
          <ListItem>
            <Left>
              <Icon name={Platform.OS === 'ios' ? 'ios-moon' : 'md-moon'} size={26} color="#fff" />
            </Left>
            <Body>
              <Text>Dark theme</Text>
            </Body>
            <Right>
              <Switch value={this.props.theme !== 'light'} onValueChange={this.changeTheme} />
            </Right>
          </ListItem>
          <ListItem>
            <Body>
              <Text>Distance from destination (km)</Text>
            </Body>
            <Right>
              <Item>
                <NumberInput
                  value={`${this.props.maxDistanceFromDestination}`}
                  onSubmitEditing={this.props.setGlobalMaxDistanceFromDestination}
                />
              </Item>
            </Right>
          </ListItem>
          <ListItem>
            <Body>
              <Text>Max number of parking lots</Text>
            </Body>
            <Right>
              <Item>
                <NumberInput
                  value={`${this.props.maxNumberOfParkingLots}`}
                  onSubmitEditing={this.props.setGlobalMaxNumberOfParkingLots}
                />
              </Item>
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    theme: state.settings.theme,
    maxDistanceFromDestination: state.settings.maxDistanceFromDestination,
    maxNumberOfParkingLots: state.settings.maxNumberOfParkingLots,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { setGlobalTheme, setGlobalMaxDistanceFromDestination, setGlobalMaxNumberOfParkingLots },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsScreen);
