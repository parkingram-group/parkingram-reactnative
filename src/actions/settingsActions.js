import { AsyncStorage } from 'react-native';

import {
  SET_ALL_GLOBAL_SETTINGS,
  SET_GLOBAL_THEME,
  SET_GLOBAL_MAX_DISTANCE_FROM_DESTINATION,
  SET_GLOBAL_MAX_NUMBER_OF_PARKING_LOTS,
} from './types';

export const setAllGlobalSettings = settings => {
  return {
    type: SET_ALL_GLOBAL_SETTINGS,
    payload: settings,
  };
};

export const initAllGlobalSettings = () => async dispatch => {
  try {
    let theme = await AsyncStorage.getItem('@MySuperStore:theme');
    let maxDistanceFromDestination = await AsyncStorage.getItem(
      '@MySuperStore:maxDistanceFromDestination'
    );
    let maxNumberOfParkingLots = await AsyncStorage.getItem('@MySuperStore:maxNumberOfParkingLots');
    if (!theme) theme = await AsyncStorage.setItem('@MySuperStore:theme', 'light');
    if (!maxDistanceFromDestination)
      maxDistanceFromDestination = await AsyncStorage.setItem(
        '@MySuperStore:maxDistanceFromDestination',
        '2'
      );
    if (!maxNumberOfParkingLots)
      maxNumberOfParkingLots = await AsyncStorage.setItem(
        '@MySuperStore:maxNumberOfParkingLots',
        '5'
      );

    maxDistanceFromDestination = parseFloat(maxDistanceFromDestination);
    maxNumberOfParkingLots = parseInt(maxNumberOfParkingLots, 10);
    dispatch({
      type: SET_ALL_GLOBAL_SETTINGS,
      payload: { theme, maxDistanceFromDestination, maxNumberOfParkingLots },
    });
  } catch (error) {
    console.error(error);
  }
};

export const setGlobalTheme = theme => {
  try {
    AsyncStorage.setItem('@MySuperStore:theme', theme);
  } catch (error) {
    console.error(error);
  }
  return {
    type: SET_GLOBAL_THEME,
    payload: theme,
  };
};

export const setGlobalMaxDistanceFromDestination = value => {
  try {
    AsyncStorage.setItem('@MySuperStore:maxDistanceFromDestination', value);
  } catch (error) {
    console.error(error);
  }
  return {
    type: SET_GLOBAL_MAX_DISTANCE_FROM_DESTINATION,
    payload: value,
  };
};

export const setGlobalMaxNumberOfParkingLots = value => {
  try {
    AsyncStorage.setItem('@MySuperStore:maxNumberOfParkingLots', value);
  } catch (error) {
    console.error(error);
  }
  return {
    type: SET_GLOBAL_MAX_NUMBER_OF_PARKING_LOTS,
    payload: value,
  };
};
