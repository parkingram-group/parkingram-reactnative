import {
  SET_PARKING_LOT,
  SET_USER_ALERTED,
  SET_ARRIVED_TO_PARKING_LOT,
  SUBSCRIBE_TO_PARKING_LOT_REQUEST,
  SEND_PARKING_LOT_CROWDEDNESS,
  UNSUBSCRIBE_TO_PARKING_LOT_REQUEST,
} from './types';

export const setParkingLot = (oldParkingLot, newParkingLot) => async dispatch => {
  if (newParkingLot) {
    dispatch({
      type: SUBSCRIBE_TO_PARKING_LOT_REQUEST,
      payload: newParkingLot.room,
    });
  }
  if (oldParkingLot) {
    dispatch({
      type: UNSUBSCRIBE_TO_PARKING_LOT_REQUEST,
      payload: oldParkingLot.room,
    });
  }

  dispatch({
    type: SET_PARKING_LOT,
    payload: newParkingLot,
  });
};

export const sendParkingLotCrowdedness = (room, crowdedness) => {
  return {
    type: SEND_PARKING_LOT_CROWDEDNESS,
    payload: { room, crowdedness },
  };
};

export const setUserAlerted = value => {
  return {
    type: SET_USER_ALERTED,
    payload: value,
  };
};

export const setArrivedToParkingLot = value => {
  return {
    type: SET_ARRIVED_TO_PARKING_LOT,
    payload: value,
  };
};
