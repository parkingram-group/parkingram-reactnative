import { Location } from 'expo';
import { Dimensions } from 'react-native';
import {
  GET_CURRENT_LOCATION_ERROR,
  GET_CURRENT_LOCATION_SUCCESS,
  SEND_DESTINATION_TO_SERVER_REQUEST,
} from './types';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = ASPECT_RATIO * LATITUDE_DELTA;

export const getCurrentLocation = () => async dispatch => {
  try {
    const isLocationEnabled = await Location.hasServicesEnabledAsync();

    if (!isLocationEnabled) {
      dispatch({
        type: GET_CURRENT_LOCATION_ERROR,
        payload: 'The application needs permission to access the location.',
      });
    } else {
      const currentLocation = await Location.getCurrentPositionAsync({
        accuracy: Location.Accuracy.BestForNavigation,
      });

      const region = {
        latitude: currentLocation.coords.latitude,
        longitude: currentLocation.coords.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      };

      dispatch({
        type: GET_CURRENT_LOCATION_SUCCESS,
        payload: region,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_CURRENT_LOCATION_ERROR,
      payload: error,
    });
  }
};

export const setRegion = coordinates => {
  const region = {
    latitude: coordinates.latitude,
    longitude: coordinates.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  };

  return {
    type: GET_CURRENT_LOCATION_SUCCESS,
    payload: region,
  };
};

export const getCurrentLocationAndSendToServer = (
  maxDistanceFromDestination,
  maxNumberOfParkingLots
) => async dispatch => {
  try {
    const isLocationEnabled = await Location.hasServicesEnabledAsync();

    if (!isLocationEnabled) {
      dispatch({
        type: GET_CURRENT_LOCATION_ERROR,
        payload: 'The application needs permission to access the location.',
      });
    } else {
      const currentLocation = await Location.getCurrentPositionAsync({
        accuracy: Location.Accuracy.BestForNavigation,
      });

      const { latitude, longitude } = currentLocation.coords;

      const region = {
        latitude,
        longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      };
      const message = {
        maxDistanceFromDestination,
        latitude,
        longitude,
        maxNumberOfParkingLots,
      };

      dispatch({
        type: GET_CURRENT_LOCATION_SUCCESS,
        payload: region,
      });

      dispatch({
        type: SEND_DESTINATION_TO_SERVER_REQUEST,
        payload: message,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_CURRENT_LOCATION_ERROR,
      payload: error,
    });
  }
};

export const sendDestinationToServer = (
  destination,
  currentLocation,
  maxDistanceFromDestination,
  maxNumberOfParkingLots
) => {
  const message = {
    maxDistanceFromDestination,
    destination,
    currentLocation,
    maxNumberOfParkingLots,
  };
  return {
    type: SEND_DESTINATION_TO_SERVER_REQUEST,
    payload: message,
  };
};
