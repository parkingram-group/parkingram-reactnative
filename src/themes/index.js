export default {
  light: {
    primaryBackgroundColor: '#ffffff',
    primaryTextColor: '#111111',
  },
  dark: {
    primaryBackgroundColor: '#111111',
    primaryTextColor: '#ffffff',
  },
};
