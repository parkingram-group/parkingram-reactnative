import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import SplashScreen from '../screens/FakeSplashScreen';

const Splash = createStackNavigator({ SplashScreen });

export default createAppContainer(
  createSwitchNavigator(
    {
      Splash,
      App: MainTabNavigator,
    },
    {
      initialRouteName: 'Splash',
    }
  )
);
