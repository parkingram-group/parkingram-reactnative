import React from 'react';
import { View } from 'react-native';
import { List, ListItem, Body, Text } from 'native-base';

export default class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.crowdednessToColor = [
      { key: 'empty', value: 'rgb(119, 177, 93)' },
      { key: 'few occupied places', value: 'rgb(204, 200, 86)' },
      { key: 'half full', value: 'rgb(209, 150, 101)' },
      { key: 'crowded', value: 'rgb(219, 76, 76)' },
      { key: 'full', value: 'rgb(255, 45, 45)' },
    ];
  }

  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignContent: 'center',
          backgroundColor: '#fff',
        }}
      >
        <Text style={{ textAlign: 'center' }}>How crowded is the parking lot?</Text>
        <List style={{ minWidth: '100%' }}>
          {this.crowdednessToColor.map(item => {
            return (
              <ListItem
                key={item.key}
                onPress={() => this.props.sendParkingLotCrowdedness(this.props.room, item.key)}
                style={{ backgroundColor: item.value }}
              >
                <Body>
                  <Text style={{ textAlign: 'center' }}>{item.key}</Text>
                </Body>
              </ListItem>
            );
          })}
        </List>
      </View>
    );
  }
}
