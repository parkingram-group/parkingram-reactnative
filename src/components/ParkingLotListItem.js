import React from 'react';
import { ListItem, Left, Right, Body, Text, Button } from 'native-base';

const ParkingLotListItem = props => {
  return (
    <ListItem
      onPress={() => {
        props.setRegion({
          latitude: props.details.latitude,
          longitude: props.details.longitude,
        });
      }}
    >
      <Left>
        <Text>{props.details.name}</Text>
      </Left>
      <Body>
        <Text>{props.details.distance.toFixed(2)} km</Text>
        <Text>Slots: {props.details.slots}</Text>
      </Body>
      <Right>
        <Button
          light
          onPress={() => {
            props.setParkingLot(null, props.details);
          }}
        >
          <Text>+</Text>
        </Button>
      </Right>
    </ListItem>
  );
};

export default ParkingLotListItem;
