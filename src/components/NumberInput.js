import React from 'react';
import { Alert } from 'react-native';
import { Input } from 'native-base';

export default class NumberInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: this.props.value ? this.props.value : '',
    };
  }

  onChanged = text => {
    let newInputValue = '';
    const numbers = '0123456789';
    for (let i = 0; i < text.length; i += 1) {
      if (numbers.indexOf(text[i]) > -1) {
        newInputValue += text[i];
      } else {
        Alert.alert('Wrong input', 'Only positive numbers are allowed', [], { cancelable: true });
      }
    }
    this.setState({ inputValue: newInputValue });
  };

  onSubmitEditing = () => {
    if (this.state.inputValue) {
      this.props.onSubmitEditing(this.state.inputValue);
    } else {
      Alert.alert('Empty input', 'Value was not saved', [], { cancelable: true });
    }
  };

  render() {
    return (
      <Input
        style={this.props.style}
        placeholder={this.props.placeholder}
        keyboardType="numeric"
        onChangeText={this.onChanged}
        value={this.state.inputValue}
        maxLength={3}
        onSubmitEditing={this.onSubmitEditing}
      />
    );
  }
}
