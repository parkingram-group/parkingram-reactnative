import React from 'react';
import { Location } from 'expo';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { GOOGLE_API_KEY } from '../config';
import { sendDestinationToServer, setParkingLot } from '../actions';

class GooglePlacesInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      listViewDisplayed: true,
    };
  }

  render() {
    return (
      <GooglePlacesAutocomplete
        ref={component => {
          this.input = component;
        }}
        placeholder="Search"
        minLength={2} // minimum length of text to search
        autoFocus={false}
        returnKeyType="search" // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        keyboardAppearance="light" // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
        // listViewDisplayed="auto" // true/false/undefined
        listViewDisplayed={this.state.listViewDisplayed}
        fetchDetails
        renderDescription={row => row.description} // custom description render
        onPress={async (data, details = null) => {
          // 'details' is provided when fetchDetails = true
          const destination = {
            latitude: details.geometry.location.lat,
            longitude: details.geometry.location.lng,
          };

          const location = await Location.getCurrentPositionAsync({
            accuracy: Location.Accuracy.BestForNavigation,
          });

          const currentLocation = {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
          };

          this.props.setParkingLot(this.props.parkingLot, null);
          this.props.sendDestinationToServer(
            destination,
            currentLocation,
            this.props.maxDistanceFromDestination,
            this.props.maxNumberOfParkingLots
          );
          this.setState({ inputValue: '', listViewDisplayed: false });
        }}
        textInputProps={{
          onChangeText: text => {
            this.setState({ inputValue: text, listViewDisplayed: true });
          },
          value: this.state.inputValue,
        }}
        getDefaultValue={() => ''}
        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          key: GOOGLE_API_KEY,
          // language: 'en', // language of the results
          // types: '(cities)', // default: 'geocode'
        }}
        styles={{
          container: {
            top: 24,
          },
          textInputContainer: {
            width: '100%',
          },
          description: {
            fontWeight: 'bold',
            backgroundColor: '#fff',
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
          row: {
            backgroundColor: '#fff',
          },
        }}
        // currentLocation // Will add a 'Current location' button at the top of the predefined places list
        currentLocationLabel="Current location"
        nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        GoogleReverseGeocodingQuery={
          {
            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }
        }
        GooglePlacesSearchQuery={{
          // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          rankby: 'distance',
          // type: 'cafe',
        }}
        GooglePlacesDetailsQuery={{
          // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
          fields: 'formatted_address',
        }}
        filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    parkingLot: state.parkingLots.parkingLot,
    maxDistanceFromDestination: state.settings.maxDistanceFromDestination,
    maxNumberOfParkingLots: state.settings.maxNumberOfParkingLots,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ sendDestinationToServer, setParkingLot }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GooglePlacesInput);
