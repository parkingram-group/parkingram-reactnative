import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, StyleSheet, Alert } from 'react-native';
import { MapView, Location } from 'expo';
import MapViewDirections from 'react-native-maps-directions';
import getDistance from 'geolib/es/getDistance';
import Feedback from '../components/Feedback';

import {
  getCurrentLocationAndSendToServer,
  initAllGlobalSettings,
  sendParkingLotCrowdedness,
  setArrivedToParkingLot,
  setUserAlerted,
} from '../actions';
import { GOOGLE_API_KEY } from '../config';

class MapContainer extends React.Component {
  constructor(props) {
    super(props);
    this.crowdednessToPinColor = {
      unknown: 'navy',
      empty: 'green',
      'few occupied places': 'yellow',
      'half full': 'orange',
      crowded: 'tomato',
      full: 'red',
    };

    this.state = {
      currentLocation: this.props.region,
      previousRegionProp: this.props.region,
      region: this.props.region,
      speed: 0,
    };
  }

  componentDidMount() {
    Location.watchPositionAsync(
      {
        accuracy: Location.Accuracy.BestForNavigation,
        timeInterval: 1000,
        distanceInterval: 30,
      },
      location => {
        const { latitude, longitude, speed } = location.coords;
        const { latitudeDelta, longitudeDelta } = this.state.region;
        const currentLocation = { latitude, longitude, latitudeDelta, longitudeDelta };
        this.setState({
          currentLocation,
          speed,
        });
        this.map.animateToRegion(currentLocation, 500);
      }
    );
  }

  componentDidUpdate() {
    if (this.state.previousRegionProp !== this.props.region) {
      this.onRegionPropChange();
    }
    if (
      !this.state.region !== this.props.region &&
      this.props.parkingLot &&
      !this.state.didArrive &&
      getDistance(
        {
          latitude: this.state.currentLocation.latitude,
          longitude: this.state.currentLocation.longitude,
        },
        { latitude: this.props.parkingLot.latitude, longitude: this.props.parkingLot.longitude }
      ) < 70 &&
      this.state.speed < 10
    ) {
      this.userArrived();
    } else if (
      this.props.parkingLot &&
      this.props.parkingLot.crowdedness === 'full' &&
      !this.props.alerted
    ) {
      this.props.setUserAlerted(true);
      Alert.alert(
        'Parking Lot Full!',
        'You may want to select a different parking lot.',
        [{ text: 'OK' }],
        {
          cancelable: true,
        }
      );
    }
  }

  userArrived() {
    this.props.setArrivedToParkingLot(true);
  }

  onRegionPropChange() {
    this.setState({
      region: this.props.region,
      previousRegionProp: this.props.region,
    });
    this.map.animateToRegion(this.props.region, 1000);
  }

  onRegionChange(region) {
    this.setState({
      region,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          ref={component => {
            this.map = component;
          }}
          // customMapStyle={this.props.theme === 'light' ? MapStyles.lightTheme : MapStyles.darkTheme}
          provider={MapView.PROVIDER_GOOGLE}
          style={styles.map}
          showsUserLocation
          showsMyLocationButton={false}
          showsCompass
          zoomEnabled
          initialRegion={this.state.region}
          onRegionChange={region => this.onRegionChange(region)}
        >
          {!this.props.parkingLot &&
            this.props.results.map(result => {
              const location = { latitude: result.latitude, longitude: result.longitude };
              const pinColor = this.crowdednessToPinColor[result.crowdedness];
              return (
                <MapView.Marker
                  key={`${result.room}-${pinColor}`}
                  coordinate={location}
                  title={result.name}
                  description={`Crowdedness: ${result.crowdedness}`}
                  pinColor={pinColor}
                />
              );
            })}
          {this.props.parkingLot && (
            <MapView.Marker
              key={`${this.props.parkingLot.room}-${
                this.crowdednessToPinColor[this.props.parkingLot.crowdedness]
              }`}
              coordinate={{
                latitude: this.props.parkingLot.latitude,
                longitude: this.props.parkingLot.longitude,
              }}
              title={this.props.parkingLot.name}
              description={`Crowdedness: ${this.props.parkingLot.crowdedness}`}
              pinColor={this.crowdednessToPinColor[this.props.parkingLot.crowdedness]}
            />
          )}
          {this.props.parkingLot && (
            <MapViewDirections
              origin={this.state.currentLocation}
              destination={this.props.parkingLot}
              apikey={GOOGLE_API_KEY}
              strokeWidth={3}
              strokeColor="hotpink"
            />
          )}
        </MapView>
        {this.props.didArrive && (
          <Feedback
            room={this.props.parkingLot.room}
            sendParkingLotCrowdedness={this.props.sendParkingLotCrowdedness}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    region: state.location.region,
    error: state.location.error,
    results: state.parkingLots.parkingLotResults,
    parkingLot: state.parkingLots.parkingLot,
    didArrive: state.parkingLots.didArrive,
    alerted: state.parkingLots.alerted,
    maxDistanceFromDestination: state.settings.maxDistanceFromDestination,
    maxNumberOfParkingLots: state.settings.maxNumberOfParkingLots,
    theme: state.settings.theme,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getCurrentLocationAndSendToServer,
      initAllGlobalSettings,
      sendParkingLotCrowdedness,
      setArrivedToParkingLot,
      setUserAlerted,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapContainer);

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    top: 64,
  },
});
