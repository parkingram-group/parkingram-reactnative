import React from 'react';
import { View, ScrollView } from 'react-native';
import { List } from 'native-base';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ParkingLotListItem from '../components/ParkingLotListItem';

import { setRegion, setParkingLot } from '../actions';

const ParkingLotList = props => {
  return (
    <View>
      <ScrollView style={{ flex: 0, maxHeight: 200 }}>
        <List style={{ backgroundColor: 'rgba(250, 250, 250, 1)' }}>
          {props.results.map(result => (
            <ParkingLotListItem
              key={result.room}
              details={result}
              setRegion={props.setRegion}
              setParkingLot={props.setParkingLot}
            />
          ))}
        </List>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    results: state.parkingLots.parkingLotResults,
    theme: state.settings.theme,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setRegion,
      setParkingLot,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ParkingLotList);
