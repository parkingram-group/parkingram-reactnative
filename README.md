# Parkingram Mobile Application

## Overview

Mobile application created with Expo (React Native) that helps in real time tracking of parking lots with the power of crowdsourcing. It provides information on the crowdedness levels and directions to registered parking lots.

## Installation

- `npm install expo-cli`
- `npm install`
- In the `config/index.js` file, you must add the Google API key, and the url where the main server is found
- `expo start`

## Screenshots

<img src="./screenshots/mobile_0.png" alt="main" width="32%"/>
<img src="./screenshots/mobile_1.png" alt="settings"  width="32%"/>
<img src="./screenshots/mobile_2.png" alt="search"  width="32%"/>
<img src="./screenshots/mobile_3.png" alt="results"  width="32%"/>
<img src="./screenshots/mobile_4.png" alt="directions"  width="32%"/>
<img src="./screenshots/mobile_5.png" alt="directions-zoomed"  width="32%"/>
<img src="./screenshots/mobile_6.png" alt="parking-lot-full"  width="32%"/>
<img src="./screenshots/mobile_7.png" alt="feedback"  width="32%"/>