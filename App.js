import React from 'react';
import { YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/store';
import AppNavigator from './src/navigation/AppNavigator';

YellowBox.ignoreWarnings([
  'Warning: Failed prop type: Invalid prop `region.latitude` of type `object` supplied to `MapView`, expected `number`.',
  'Unrecognized WebSocket connection option',
]);
const App = () => (
  <Provider store={store}>
    <AppNavigator />
  </Provider>
);

export default App;
